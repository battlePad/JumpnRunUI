﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Player : MonoBehaviour
{
    public GameObject enemy;

    public Animator anim;

    public float RotationSpeed = 100f;
    public float RunSpeed = 5f;
    public float WalkSpeed = 8f;
    public float JumpSpeed = 5f;
    public float EnemyBounce = 3f;
    public float Gravity = -2.81f;
    public int lives = 3; //current number of lives
    public int score = 0; //current score of the player

    private CharacterController characterControler = null;
    private Vector3 move = Vector3.zero;
    private Vector3 gravity = Vector3.zero;
    private float rotation = 0f;

    private bool jump = false;

    private enum AnimState
    {
        Idle,
        Running,
        Jumping
    };
    private AnimState currentState = AnimState.Idle;

    private Vector3 startPosition;
    private Quaternion startRotation;

    // Use this for initialization
    void Start()
    {
        characterControler = this.GetComponent<CharacterController>();
        if (characterControler == null)
        {
            Debug.LogError("charactetController is missing in Player.cs");
        }
        characterControler.detectCollisions = true;

        startPosition = this.transform.position;
        startRotation = this.transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        move = Mathf.Abs(Input.GetAxis("Horizontal")) * this.transform.forward * Time.deltaTime;
       
        if(Input.GetAxis("Horizontal") > 0f)
        {
            this.transform.rotation = Quaternion.Euler(0f, 90f, 0f);
        }
        else if(Input.GetAxis("Horizontal") < 0f)
        {
            this.transform.rotation = Quaternion.Euler(0f, -90f, 0f);
        }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            move *= RunSpeed;
        }
        else
        {
            move *= WalkSpeed;
        }
        if (!characterControler.isGrounded)
        {
            gravity += new Vector3(0f, Gravity, 0f) * Time.deltaTime;
        }
        else
        {
            if (jump)
            {
                gravity = Vector3.zero;
                gravity.y = JumpSpeed;
                jump = false;
            }
        }

        move += gravity;

        if (Input.GetButtonDown("Jump") && characterControler.isGrounded)
        {
            jump = true;
           
        }
       
        characterControler.Move(move);

        // we do not want to collide and change our z vlaue
        if (transform.position.z != 0f)
        {
            Vector3 newPosition = this.transform.position;
            newPosition.z = 0f;
            this.transform.position = newPosition;
        }

        UpdateAnimation();
    }

    void OnControllerColliderHit(ControllerColliderHit col)
    {
        if (col.gameObject.tag == "Death")
        {
            PlayerDied();
        }
        else if (col.gameObject.tag == "Head")
        {
            col.transform.parent.gameObject.SetActive(false);
            gravity.y = 0f;
            characterControler.Move(Vector3.up * EnemyBounce);
        }
    }

    public void PlayerDied()
    {
        enemy.GetComponent<Enemy>().Reset();
        this.Reset();
    }

    private void Reset()
    {
        this.transform.position = startPosition;
        this.transform.rotation = startRotation;
    }

    void UpdateAnimation()
    {
        if(jump)
        {
            SwitchAnim(AnimState.Jumping);
        }
 
        if (Mathf.Approximately(move.x, 0f))
        {
            SwitchAnim(AnimState.Idle);
        }
        else
        {
            SwitchAnim(AnimState.Running);
        }

    }

    void SwitchAnim(AnimState state)
    {   
        switch (state)
        {
            case AnimState.Idle:
                anim.SetBool("isRunning", false);
                anim.SetBool("isIdle", true);
                break;
            case AnimState.Running:
                anim.SetBool("isRunning", true);
                anim.SetBool("isIdle", false);
                break;
            case AnimState.Jumping:
                anim.SetTrigger("isJumping");
                break;
        }
    }
}
