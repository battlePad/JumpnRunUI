﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    public float playerSpeed = 5f;

    private GameObject player = null;
    private Vector3 move = Vector3.zero;

    private Vector3 startPosition;
    private Quaternion startRotation;

    // Use this for initialization
    void Start () {

        player = GameObject.FindGameObjectWithTag("Player");

        if(player == null)
        {
            Debug.LogError("Player is null in Enemy.cs");
        }

        startPosition = this.transform.position;
        startRotation = this.transform.rotation;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(player.transform.position.x <= this.transform.position.x)
        {     
            this.transform.rotation = Quaternion.Euler(new Vector3(0f, 270f, 0f));
        }
        else
        {   
            this.transform.rotation = Quaternion.Euler(new Vector3(0f, 90f, 0f));
        }

        move = Vector3.forward * playerSpeed * Time.deltaTime;

        this.transform.Translate(move);

        if (transform.position.z != 0f)
        {
            Vector3 newPosition = this.transform.position;
            newPosition.z = 0f;
            this.transform.position = newPosition;
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Player")
        {
            col.gameObject.GetComponent<Player>().PlayerDied();
        }
    }

    public void Reset()
    {
        this.gameObject.SetActive(true);
        this.transform.position = startPosition;
        this.transform.rotation = startRotation;
    }

}
