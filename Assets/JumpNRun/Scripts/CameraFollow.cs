﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

    public GameObject Player;

    private float distanceToPlayer = 0f;

	// Use this for initialization
	void Start () {
        distanceToPlayer = Vector3.Distance(this.transform.position, Player.transform.position);

	}
	
	// Update is called once per frame
	void Update () {
        this.transform.position = new Vector3(Player.transform.position.x,this.transform.position.y, - distanceToPlayer);

	}
}
